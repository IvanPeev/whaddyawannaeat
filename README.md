# Whaddya Wanna Eat

## Spinning the wheel
![Alt 1](example/1.gif)

## Adding items to the wheel
![Alt 2](example/2.gif)

## Removing items from the wheel
![Alt 3](example/3.gif)

