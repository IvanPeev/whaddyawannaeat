package com.whaddyawannaeat;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.function.Function;

public class CrudView extends View {
    private ArrayList<String> items;

    public CrudView(Context context) {
        super(context);
    }

    public CrudView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CrudView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setItems(ArrayList<String> items) {
        this.items = items;
    }

    public ArrayList<String> getItems() {
        return items;
    }

    public void onClickAdd(Function handleAfter) {
        String addTitle = getResources().getString(R.string.dialog_add_title);
        String addConfirm = getResources().getString(R.string.dialog_add_confirm);
        String addCancel = getResources().getString(R.string.dialog_add_cancel);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final EditText input = new EditText(getContext());

        DialogInterface.OnClickListener handleConfirm = (dialog, which) -> {
            if (items.size() >= 6) {
                Toast.makeText(getActivity(), "The wheel can have 6 items max", Toast.LENGTH_LONG).show();
                return;
            }

            String inputText = input.getText().toString();

            if (inputText.length() > 0) {
                String item = inputText.substring(0, 1).toUpperCase() + inputText.substring(1);
                this.items.add(item);
            }

            handleAfter.apply(null);
        };

        input.setInputType(InputType.TYPE_CLASS_TEXT);

        builder.setTitle(addTitle)
            .setView(input)
            .setPositiveButton(addConfirm, handleConfirm)
            .setNegativeButton(addCancel, null);

        AlertDialog dialog = builder.create();

        dialog.show();
    }

    public void onClickRemove(Function handleAfter) {
        String removeTitle = getResources().getString(R.string.dialog_remove_title);
        String removeConfirm = getResources().getString(R.string.dialog_remove_confirm);
        String removeCancel = getResources().getString(R.string.dialog_remove_cancel);
        DialogInterface.OnMultiChoiceClickListener handleChoice = (dialog, which, isChecked) -> {};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        CharSequence[] charItems = items.toArray(new CharSequence[items.size()]);
        boolean[] itemsChecked = new boolean[items.size()];

        DialogInterface.OnClickListener handleConfirm = (dialog, which) -> {
            int deleteCount = 0;

            for (int i = 0; i < itemsChecked.length; i++) {
                if (itemsChecked[i]) {
                    deleteCount++;
                }
            }

            if (items.size() - deleteCount < 3) {
                Toast.makeText(getActivity(), "The wheel needs to have at least 3 items", Toast.LENGTH_LONG).show();
                return;
            }

            for (int i = itemsChecked.length - 1; i >= 0; i--) {
                if (itemsChecked[i]) {
                    this.items.remove(i);
                }
            }

            handleAfter.apply(null);
        };

        builder.setTitle(removeTitle)
            .setMultiChoiceItems(charItems, itemsChecked, handleChoice)
            .setPositiveButton(removeConfirm, handleConfirm)
            .setNegativeButton(removeCancel, null);

        AlertDialog dialog = builder.create();

        dialog.show();
    }

    private Activity getActivity() {
        Context context = getContext();
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity) context;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
        return null;
    }
}
