package com.whaddyawannaeat.fragments;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.whaddyawannaeat.ItemsViewModel;
import com.whaddyawannaeat.R;
import com.whaddyawannaeat.WheelView;

public class WheelFragment extends Fragment implements WheelView.OnRotationListener<String> {
    private WheelView wheelView;
    private Button buttonRotate;
    private ItemsViewModel viewModel;
    private MediaPlayer spinningMP;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_wheel, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity()).get(ItemsViewModel.class);

        wheelView = view.findViewById(R.id.wheel);
        buttonRotate = view.findViewById(R.id.rotate);
        spinningMP = MediaPlayer.create(getActivity(), R.raw.spinning);

        wheelView.setOnRotationListener(this);
        wheelView.setItems(viewModel.get());
        buttonRotate.setOnClickListener(v -> {
            float maxAngle = (float) Math.floor(Math.random() * 40) + 10f;
            long duration = 5000;
            long interval = 1000 / 60; // every 1000 / 60 = 16.67ms (60FPS) render rotation

            if (spinningMP.isPlaying()) {
                spinningMP.stop();
                spinningMP = MediaPlayer.create(getActivity(), R.raw.spinning);
            }
            spinningMP.start();
            wheelView.rotate(maxAngle, duration, interval);
        });
    }

    @Override
    public void onRotation() {
        Log.d("XXXX", "On Rotation");
    }

    @Override
    public void onStopRotation(String item) {
        Toast.makeText(getActivity(), item, Toast.LENGTH_LONG).show();
    }
}
