package com.whaddyawannaeat.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.whaddyawannaeat.CrudView;
import com.whaddyawannaeat.ItemsViewModel;
import com.whaddyawannaeat.R;

import java.util.ArrayList;
import java.util.function.Function;

public class CrudFragment extends Fragment {
    private ItemsViewModel viewModel;
    private CrudView crudView;
    private ImageView addButton;
    private ImageView removeButton;
    private ListView listItems;
    private ArrayAdapter<String> adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_crud, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity()).get(ItemsViewModel.class);

        crudView = view.findViewById(R.id.crud);
        addButton = view.findViewById(R.id.button_add);
        removeButton = view.findViewById(R.id.button_remove);
        listItems = view.findViewById(R.id.list_items);

        adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, viewModel.get());

        crudView.setItems(viewModel.get());
        listItems.setAdapter(adapter);

        addButton.setOnClickListener(v -> {
            Function handleAfter = (a) -> {
                ArrayList<String> newItems = crudView.getItems();
                viewModel.set(newItems);
                adapter.notifyDataSetChanged();
                return null;
            };

            crudView.onClickAdd(handleAfter);
        });

        removeButton.setOnClickListener(v -> {
            Function handleAfter = (a) -> {
                ArrayList<String> newItems = crudView.getItems();
                viewModel.set(newItems);
                adapter.notifyDataSetChanged();
                return null;
            };

            crudView.onClickRemove(handleAfter);
        });
    }
}
