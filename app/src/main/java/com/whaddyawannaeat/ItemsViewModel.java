package com.whaddyawannaeat;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

public class ItemsViewModel extends AndroidViewModel {
    private final MutableLiveData<ArrayList<String>> items = new MutableLiveData<>();

    public ItemsViewModel(@NonNull Application application) {
        super(application);
        String[] defaultItems = getApplication().getResources().getStringArray(R.array.food_list);
        ArrayList<String> listItems = new ArrayList<>();

        for (String defaultItem : defaultItems) {
            listItems.add(defaultItem);
        }

        items.setValue(listItems);
    }

    public ArrayList<String> get() {
        return items.getValue();
    }

    public void set(ArrayList<String> items) {
        this.items.setValue(items);
    }
}
